# bue 2017-01-30

# python stadard library
import copy
import json
import re
import sys

# acpipe library
import acpipe_acjson.acjson as ac

# const
# ts_ABC = (
#    None,"A","B","C","D","E","F","G","H","I",
#    "J","K","L","M","N","O","P","Q","R",
#    "S","T","U","V","W","X","Y","Z")

# func
def pgmparser(
        d_inacjson,
        d_bufferacjson,
        d_outacjson,
        s_pgmpathfile,
        s_instation,
        s_bufferstation,
        s_pipettetipstation,
        s_outstation,
        s_acaxis
    ):
    """
    input:
        d_inacjson: input acjson e.g. drug acjson dictionary with begin concentration
        d_bufferacjson: buffer acjson dictionary e.g. dmso acjson
        s_pgmpathfile: biotek pgm file to process

        s_instation: input station e.g. "E"
        s_bufferstation: dilusion buffer staton e.g. "D"
        s_pipettetipstation: tip load staton e.g. "A"
        s_outstation: output staton e.g. "B"

        s_acaxis: e.g. "perturbation"

    outout:
        d_outacjson: acjson dictionary to write the output into it.

    descriptin:
        transforms information stored in biotek pgm files into acjson object
    """
    print("\nLoad pgm source code file and translate into pgm dictionary of dictionary:")
    # open pgm file
    dd_pgm = {}
    i_count = 0
    b_loop = False
    with open(s_pgmpathfile, "r") as f_handle:
        # step through file
        for s_line in f_handle:
            # set variables
            b_pipette = False
            d_stationacjson = None
            s_type = None
            # parse line
            s_line = s_line.replace("\n","")
            print("\nreadline: '{}'".format(s_line))

            # ; comment or empty
            if re.search("^;.*", s_line) or re.search("^\W*$", s_line):
                pass # nop

            # PRG_HDR
            elif re.search("^PRG_HDR .*", s_line):
                pass # nop

            # PRG_END
            elif re.search("^PRG_END .*", s_line):
                pass # nop program end

            # PRG_STEP TYPE REM
            elif re.search("^PRG_STEP .* TYPE=REM .*", s_line):
                # o_text = re.match('^PRG_STEP \(STEP=(?P<s_pgmsourcecodestep>\d+), TYPE=REM \(TEXT="(?P<s_text>.*)"\)\)', s_line)
                # s_text = o_text.group("s_text")
                pass # nop remak

            # PRG_STEP TYPE DELAY
            elif re.search("^PRG_STEP .* TYPE=DELAY .*", s_line):
                pass # nop delay

            # PRG_STEP TYPE MAP
            elif re.search("^PRG_STEP .* TYPE=MAP .*", s_line):
                pass # nop

            # PRG_STEP TYPE TIP
            elif re.search("^PRG_STEP .* TYPE=TIP .*", s_line):
                # reset b_asp as pipette tips are ejected and new ones are loaded
                # what stands PRIM for?
                # PRG_STEP (STEP=33, TYPE=TIP (PRIM=F, STA=A, STRT=1, LOOP=0, LOOP2=0, LOOP3=0, SNGL=T, SLOC=P, SROW=A, INCR=F, AINC=T, P384=F, FILE="TDD", FSTR=R))
                o_pipette = re.match("^PRG_STEP \(STEP=(?P<s_pgmsourcecodestep>\d+), TYPE=TIP \(PRIM=(?P<s_prim>\w+), STA=(?P<s_station>\w+), STRT=(?P<s_column>\d+), LOOP=(?P<s_loop1>\d+), LOOP2=(?P<s_loop2>\d+), LOOP3=(?P<s_loop3>\d+), SNGL=(?P<s_singlechannel>\w+), SLOC=(?P<s_startlocation>\w+), SROW=(?P<s_row>\w+), INCR=(?P<s_incrementrow>\w+), AINC=(?P<s_autoincrement>\w+), P384=(?P<s_eigthchannel>\w+), FILE=(?P<s_file>.+), FSTR=(?P<s_fstr>\w+)\)\)", s_line)
                s_type = "tip"
                i_count += 1
                if (b_loop):
                    li_loop.append(i_count)
                b_pipette = True

            # PRG_STEP TYPE LOOP ON
            elif re.search("^PRG_STEP .* TYPE=LOOPON .*", s_line):
                # loop begin
                o_loop = re.match("^PRG_STEP \(STEP=(?P<s_pgmsourcecodestep>\d+), TYPE=LOOPON \(FIX=(?P<s_fix>\w+), CNT=(?P<s_count>\d+), SVAR=(?P<s_svar>\d+)\)\)", s_line)
                # check input
                if (o_loop.group("s_fix") != "T") :
                    sys.exit("Error : at pgm source code step {} loop number fix is not T but {}. case not yet implemented.".format(
                        o_loop.group("s_pgmsourcecodestep"), o_loop.group("s_fix"))
                    )
                if (o_loop.group("s_svar") != "0") :
                    sys.exit("Error : at pgm source code step {} SVAR is not 0 but {}. 1 stands for map location,  2 stands for todo list. this cases are not yet implemented.".format(
                        o_loop.group("s_pgmsourcecodestep"), o_loop.group("s_svar"))
                    )
                # process
                i_loopcounttotal = int(o_loop.group("s_count"))
                li_loop = []
                b_loop = True

            # PRG_STEP TYPE LOOP OFF
            elif re.search("^PRG_STEP .* TYPE=LOOPOFF .*", s_line):
                # set loop end
                b_loop = False
                # loop copy conent
                for i_loopcount in range(1,i_loopcounttotal):
                    for i_loop in li_loop:
                        i_count += 1
                        dd_pgmclone = copy.deepcopy(dd_pgm[i_loop])
                        dd_pgmclone.update({"i_loopcount": i_loopcount})
                        dd_pgm.update({i_count : dd_pgmclone})

            # PRG_STEP TYPE ASP
            elif re.search("^PRG_STEP .* TYPE=ASP .*", s_line):
                # aspirate
                o_pipette = re.match("^PRG_STEP \(STEP=(?P<s_pgmsourcecodestep>\d+), TYPE=ASP \(VOL=(?P<s_volume>\d+), STA=(?P<s_station>\w+), STRT=(?P<s_column>\d+), LOOP=(?P<s_loop1>\d+), LOOP2=(?P<s_loop2>\d+), LOOP3=(?P<s_loop3>\d+), SNGL=(?P<s_singlechannel>\w+), SLOC=(?P<s_startlocation>\w+), SROW=(?P<s_row>\w+), INCR=(?P<s_incrementrow>\w+), AINC=(?P<s_autoincrement>\w+), P384=(?P<s_eigthchannel>\w+), FILE=(?P<s_file>.+), FSTR=(?P<s_fstr>\w+)\)\)", s_line)
                s_type = "asp"
                i_count += 1
                if (b_loop):
                    li_loop.append(i_count)
                b_pipette = True

            # PRG_STEP TYPE DISP
            elif re.search("^PRG_STEP .* TYPE=DISP .*", s_line):
                # dispense
                o_pipette = re.match("^PRG_STEP \(STEP=(?P<s_pgmsourcecodestep>\d+), TYPE=DISP \(VOL=(?P<s_volume>\d+), STA=(?P<s_station>\w+), STRT=(?P<s_column>\d+), LOOP=(?P<s_loop1>\d+), LOOP2=(?P<s_loop2>\d+), LOOP3=(?P<s_loop3>\d+), SNGL=(?P<s_singlechannel>\w+), SLOC=(?P<s_startlocation>\w+), SROW=(?P<s_row>\w+), INCR=(?P<s_incrementrow>\w+), AINC=(?P<s_autoincrement>\w+), P384=(?P<s_eigthchannel>\w+), FILE=(?P<s_file>.+), FSTR=(?P<s_fstr>\w+)\)\)", s_line)
                s_type = "disp"
                i_count += 1
                if (b_loop):
                    li_loop.append(i_count)
                b_pipette = True

            # ERROR
            else:
                sys.exit("Unknowne pgm file command in line: {}".format(s_line))

            # PRG_STEP TYPE TIP ASP DISP
            if (b_pipette):
                print("extract: {}".format(o_pipette))
                # initialize record
                d_record = {}
                d_record.update({
                    "i_pgmsourcecodestep" : int(o_pipette.group("s_pgmsourcecodestep"))
                })
                d_record.update({"s_type" : s_type})
                d_record.update({"i_loopcount" : 0})

                # handle PRIM part of step type tip
                try:
                    s_prim = o_pipette.group("s_prim")
                    if (s_prim != "F"):
                        sys.exit("Error : tip PRIM {} is not F. no idea waht prim stands for. case not yet implemented.".format(
                            s_prim
                        ))
                except IndexError:
                    pass

                # handle VOL part of step type asp and disp
                try:
                    d_record.update({
                        "volume" : float(o_pipette.group("s_volume"))
                    })
                    d_record.update({"volumeUnit" : "uL"})
                except IndexError:
                    pass

                # handle STA input station
                d_record.update({"s_station": o_pipette.group("s_station")})
                # get acjson
                d_stationacjson = None
                if (d_record["s_station"] == s_instation):
                    d_stationacjson = d_inacjson
                elif (d_record["s_station"] == s_outstation):
                    d_stationacjson = d_outacjson
                elif (d_record["s_station"] == s_bufferstation):
                    d_stationacjson = d_bufferacjson
                elif (d_record["s_station"]  == s_pipettetipstation):
                    d_stationacjson = {}
                    d_stationacjson.update({"welllayout":"1x1"})
                else:
                    sys.exit("Error : undefined station detected {}. input station was spcified as {} output station as {} buffer station as {} and pipette tip station as {}".format(
                        d_record["s_station"],
                        s_instation,
                        s_outstation,
                        s_bufferstation,
                        s_pipettetipstation
                    ))

                # handle STRT column
                d_record.update({"i_column": int(o_pipette.group("s_column"))})

                # handle SNGL single and multi channel
                if (o_pipette.group("s_singlechannel") != "T"):
                    sys.exit("Error : this is not T single channel code {}. multi channel case not yet implemented.".format(
                        o_pipette.group("s_singlechannel")
                    ))

                # handel SLOC starting location
                # bue 20170225: step type tip utilizes F fix and P previos
                if (o_pipette.group("s_startlocation") != "F") and (s_type != "tip"):
                    sys.exit("Error : this starting location is not F fix {}. A ask P previos L multilocationmap case not implemented.".format(
                        o_pipette.group("s_startlocation")
                    ))

                # handel SROW row
                d_record.update({"i_row" : ac.ts_ABC.index(o_pipette.group("s_row"))})

                # handle LOOP and AINC increments
                if (o_pipette.group("s_autoincrement") == "T") :
                    i_stepsize = 1  # step size 1
                else:
                    i_stepsize = int(o_pipette.group("s_loop1")) + int(o_pipette.group("s_loop2")) + int(o_pipette.group("s_loop3"))
                # handle INCR
                if (o_pipette.group("s_incrementrow") != "T"):
                    i_stepsize = i_stepsize * int(d_stationacjson["welllayout"].split("x")[1])
                # step size
                d_record.update({"i_stepsize": i_stepsize})

                # handle p384 single channel mimic 8 channel pattern
                if (o_pipette.group("s_eigthchannel") != "F") :
                    sys.exit("Error : single channel mimic 8 channel pattern setting is not F false {}. case not yet implemented.".format(
                        o_pipette.group("s_eigthchannel")
                    ))

                # handle FILE file pipete instruction
                # not yet implemented

                # handle FSTR fstr
                if (o_pipette.group("s_fstr") != "R") :
                    sys.exit("Error : this was till now always R. no idea waht fstr stands for. case not yet implemented.".format(
                        o_pipette.group("s_fstr")
                    ))

                # map to dd_pgm
                dd_pgm.update({i_count : copy.deepcopy(d_record)})

    # pack acjson
    print("\nProcess pgm dictionary of dictionary:")
    li_pgmstep = list(dd_pgm.keys())
    li_pgmstep.sort()
    b_asp = False
    for i_pgmstep in li_pgmstep:
        # set variable
        d_record = dd_pgm[i_pgmstep]
        d_stationacjson = None
        # get station
        if (d_record["s_station"] == s_instation):
            d_stationacjson = d_inacjson
        elif (d_record["s_station"] == s_outstation):
            d_stationacjson = d_outacjson
        elif (d_record["s_station"] == s_bufferstation):
            d_stationacjson = d_bufferacjson
        elif (d_record["s_station"] == s_pipettetipstation):
            d_stationacjson = {}
            d_stationacjson.update({"welllayout":"1x1"})
        else:
            sys.exit("Error : at pgm source code step {} undefined station detected {}. input station was spcified as {} output station as {} buffer station as {} and pipette tip station as {}".format(
                d_record["i_pgmsourcecodestep"],
                d_record["s_station"],
                s_instation,
                s_outstation,
                s_bufferstation,
                s_pipettetipstation
            ))
        # get real coordinate
        i_coor = ac.xy2icoor(i_x=d_record["i_column"], i_y=d_record["i_row"], s_welllayout=d_stationacjson["welllayout"])
        i_coor = i_coor + d_record["i_loopcount"] * d_record["i_stepsize"]
        # print info
        print("\nDD_PGM : process pgm {} real step {} source code step {} type {} station {} coordinate {}".format(
            s_pgmpathfile.split("/")[-1],
            i_pgmstep, d_record["i_pgmsourcecodestep"],
            d_record["s_type"],
            d_record["s_station"],
            i_coor
        ))
        # tip or aspire or dispense
        if (d_record["s_type"] == "tip"):
            # reset d_transit
            d_aspvolume = {}
            d_aspelement = {}
            b_asp = False

        elif (d_record["s_type"] == "asp"):
            # check for consecutive aspire
            if (b_asp):
                sys.exit("Error : at pgm source code step {} code seems to consecutive aspire. parser can't handle that.".format(
                    d_record["i_pgmsourcecodestep"]
                ))
            else:
                b_asp = True
            # reset d_transit
            d_aspvolume = {}
            d_aspvolume.update({"volume" : copy.deepcopy(d_record["volume"])})
            d_aspvolume.update({
                "volumeUnit" : copy.deepcopy(d_record["volumeUnit"])
            })
            # get element
            d_aspelement = copy.deepcopy(d_stationacjson[str(i_coor)][s_acaxis])
            print("asp coordinate: {}\nasp axis: {}\nasp element: {}".format(i_coor, s_acaxis, d_aspelement))
            # if asp station is output station update volume
            if (d_record["s_station"] == s_outstation):
                if (d_outacjson[str(i_coor)]["volumeUnit"] != d_record["volumeUnit"]):
                    sys.exit("Error: at pgm source code step {} coordinate {} asp volume unit {} differs form original well unit {}. this should never happen.".format(
                        d_record["i_pgmsourcecodestep"],
                        i_coor, d_outacjson[str(i_coor)]["volumeUnit"],
                        d_record["volumeUnit"]
                    ))
                # calucation
                d_outacjson[str(i_coor)].update({
                    "volume": d_outacjson[str(i_coor)]["volume"] - d_record["volume"]
                })

        elif (d_record["s_type"] == "disp"):
            # check aspire flag
            if not (b_asp):
                sys.exit("Error : at pgm source code step {} can not dispense, it seems nothing or not enough was aspired.".format(
                    d_record["i_pgmsourcecodestep"]
                ))

            # get station
            if (d_record["s_station"] != s_outstation):
                sys.exit("Error : at pgm source code step {} this code seems to dispense into other plate {} then output plate {}. parser can't handle that.".format(
                    d_record["i_pgmsourcecodestep"],
                    d_record["s_station"],
                    s_outstation
                ))
            # get element
            if (d_outacjson[str(i_coor)][s_acaxis] == None):
                d_element = {}
            else:
                d_element = d_outacjson[str(i_coor)][s_acaxis]
            # get volume
            d_ovolume = {}
            # well without content
            try:
                r_volume = d_outacjson[str(i_coor)]["volume"]
                d_ovolume.update({"volume" : r_volume})
                d_ovolume.update({
                    "volumeUnit" : d_outacjson[str(i_coor)]["volumeUnit"]
                })
            # well with content
            except KeyError:
                d_ovolume.update({"volume" : 0})
                d_ovolume.update({"volumeUnit" : "uL"})
            # calculate volume unit
            if not((d_ovolume["volumeUnit"] == None)
                   or (d_ovolume["volumeUnit"] == d_aspvolume["volumeUnit"])):
                sys.exit("Error : at pgm source code step {} output coordinate {} two different volume unit for same well detetcted {} {}. parser can't handle that.".format(
                    d_record["i_pgmsourcecodestep"],
                    i_coor,
                    d_aspvolume["volumeUnit"],
                    d_ovolume["volumeUnit"]
                ))
            s_volumeunit = d_aspvolume["volumeUnit"]
            # calculat pipetted volume
            r_aspvolume = d_aspvolume["volume"] - d_record["volume"]
            if (r_aspvolume < 0):
                r_volume = d_record["volume"]
                d_aspvolume["volume"] = 0
                b_asp = False # reset aspire flag
            else:
                r_volume = d_record["volume"]
                d_aspvolume["volume"] = r_aspvolume
            # calculate total volume
            r_volumetotal = d_ovolume["volume"] + r_volume
            d_volume = {}
            d_volume.update({"volume": r_volumetotal})
            d_volume.update({"volumeUnit": s_volumeunit})
            # merge elements
            print("disp element: {}".format(d_aspelement))
            ls_element = list(d_aspelement.keys())
            for s_element in ls_element:
                # asp element already in output well
                try:
                    d_element[s_element]
                    # unit check
                    if (d_element["volumeUnit"] != d_aspelement["volumeUnit"]):
                        sys.exit("Error: at pgm source code step {} aspire well volume unit {} differs from dispense well {} unit {}. this should never happen.".format(
                            d_record["i_pgmsourcecodestep"],
                            d_outacjson[str(i_coor)]["volumeUnit"],
                            i_coor, d_record["volumeUnit"]
                        ))
                    # calulat concentration
                    r_newvolumetotal = d_element["volume"] + d_aspelement["volume"]
                    r_newconc = (d_element[s_element]["conc"] * (d_element["volume"] / r_newvolumetotal)) + (d_aspelement[s_element]["conc"] * (d_aspelement["volume"] / r_newvolumetotal))
                    d_aspelement[s_element].update({"conc" : r_newconc})
                # asp element not yet in output well
                except KeyError:
                    pass
                # update output well
                d_element.update({s_element : d_aspelement[s_element]})

            # concentration calculation
            ls_element = list(d_element.keys())
            for s_element in ls_element:
                r_conc = d_element[s_element]["conc"] * (r_volume / r_volumetotal)
                d_element[s_element].update({"conc" : r_conc})
            # volume and element update
            for s_key, o_value in d_volume.items():
                d_outacjson[str(i_coor)].update({s_key : o_value})
            # write to acjson
            d_outacjson = ac.acfuseaxisrecord(
                d_acjson=d_outacjson,
                s_coor=str(i_coor),
                s_axis=s_acaxis,
                d_record=d_element,
                s_ambiguous="update",
                b_deepcopy=True
            )
        else:
            sys.exit("Error : in {} at pgm source code step {} unknowne s_type detected {}.".format(
                d_record,
                d_record["i_pgmsourcecodestep"],
                d_record["s_type"]
            ))
    # output
    return(d_outacjson)


def biotek2addacjson(
        s_runid,
        s_inacjson,
        s_bufferacjson,
        s_pgmpathfile,
        s_outacjson,
        s_acaxis="perturbation",
        s_opath="./",
        s_instation="E",
        s_bufferstation="D",
        s_pipettetipstation="A",
        s_outstation="B",
        s_runtype="acpipe_biotek"
    ):
    """
    """
    # reset output
    d_oacjson = None

    # handle buffer acjson file
    with open(s_bufferacjson,"r") as f_json:
        d_bufferacjson = json.load(f_json)

    # handle input acjson file
    with open(s_inacjson,"r") as f_json:
        d_inacjson = json.load(f_json)

    # handle output acjson
    with open(s_outacjson,"r") as f_json:
        d_outacjson = json.load(f_json)

    # parse pgm file and pack into acjson
    d_oacjson = pgmparser(
        d_inacjson=d_inacjson,
        d_bufferacjson=d_bufferacjson,
        d_outacjson=d_outacjson,
        s_pgmpathfile=s_pgmpathfile,
        s_instation=s_instation,
        s_bufferstation=s_bufferstation,
        s_pipettetipstation=s_pipettetipstation,
        s_outstation=s_outstation,
        s_acaxis=s_acaxis)

    # handle log
    s_log = "{}; {}; {}; {} | biotek2addacjson > {}".format(
        d_oacjson["log"],
        s_inacjson.split("/")[-1],
        s_bufferacjson.split("/")[-1],
        s_pgmpathfile.split("/")[-1],
        d_oacjson["acid"]
    )
    d_oacjson.update({"log" : s_log})

    # output json file
    with open(s_opath + d_oacjson["acid"], "w") as f_json:
        json.dump(d_oacjson, f_json, indent=4, sort_keys=True)
    # return acjson object
    return(d_oacjson)


def biotek2newacjson(
        s_runid,
        s_inacjson,
        s_bufferacjson,
        s_pgmpathfile,
        s_outwelllayout="8x12",
        s_acaxis="perturbation",
        s_opath="./",
        s_instation="E",
        s_bufferstation="D",
        s_pipettetipstation="A",
        s_outstation="B",
        s_runtype="acpipe_biotek"
    ):
    """
    s_welllayout
    ("0x0", "not_yet_specified or not_available"),
    ("1x1", "1 Well Tube or Petridish"),
    ("1x4", "4 Wellplate"),
    ("2x4", "8 Wellplate"),
    ("8x12", "96 Wellplate"),
    ("16x24", "384 Wellplate"),
    """
    # reset output
    d_oacjson = None

    # handle buffer acjson file
    with open(s_bufferacjson,"r") as f_json:
        d_bufferacjson = json.load(f_json)

    # handle input acjson file
    with open(s_inacjson,"r") as f_json:
        d_inacjson = json.load(f_json)

    # handle output acjson
    d_outacjson = ac.acbuild(
        s_welllayout=s_outwelllayout,
        s_runid=s_runid,
        s_runtype=s_runtype)

    # parse pgm file and pack into acjson
    d_oacjson = pgmparser(
        d_inacjson=d_inacjson,
        d_bufferacjson=d_bufferacjson,
        d_outacjson=d_outacjson,
        s_pgmpathfile=s_pgmpathfile,
        s_instation=s_instation,
        s_bufferstation=s_bufferstation,
        s_pipettetipstation=s_pipettetipstation,
        s_outstation=s_outstation,
        s_acaxis=s_acaxis
    )

    # handle log
    s_log = "{}; {}; {} | biotek2newacjson > {}".format(
        s_inacjson.split("/")[-1],
        s_bufferacjson.split("/")[-1],
        s_pgmpathfile.split("/")[-1],
        d_oacjson["acid"]
    )
    d_oacjson.update({"log" : s_log})

    # output json file
    with open(s_opath + d_oacjson["acid"], "w") as f_json:
        json.dump(d_oacjson, f_json, indent=4, sort_keys=True)
    # return acjson object
    return(d_oacjson)


if __name__ == "__main__":
    # execute only if run as a script
    biotek2newacjson(
        s_runid = "motherplate",
        s_inacjson="../acpipe_acjson/drugset1a_ac.json",
        s_bufferacjson="../acpipe_acjson/bufferset1_ac.json",
        s_pgmpathfile="MotherPlate1_20131216.PGM",
        s_outwelllayout="8x12",
        s_acaxis="perturbation",
        s_opath="./",
        s_instation="E",
        s_bufferstation="D",
        s_pipettetipstation="A",
        s_outstation="B",
        s_runtype="acpipe_biotek"
    )
